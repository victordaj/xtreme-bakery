module.exports = {
    orders: require('./orders/router'),
    users: require('./users/router'),
    products: require('./products/router'),
    auth: require('./auth/router')
}