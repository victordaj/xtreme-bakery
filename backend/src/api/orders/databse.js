const { Order } = require("../../database/models");

module.exports = {
  getAll: (options) => {
    let query = {};
    if (options && options.search && options.search.length) {
      query.name = new RegExp(options.search, "i");
    }
    if (options && options.filter && options.filter.length) {
      query.status = new RegExp(options.filter, "i");
    }

    return Order.find({ ...query })
      .skip(options.from)
      .limit(options.limit)
      .populate({ path: "products", populate: { path: "product" } })
      .lean()
      .exec();
  },
  getOne: (id) =>
    Order.findById(id)
      .populate({ path: "products", populate: { path: "product" } })
      .lean()
      .exec(),
  update: (id, order) =>
    Order.findByIdAndUpdate(id, { ...order })
      .lean()
      .exec(),
  delete: (id) => Order.findByIdAndDelete(id),
  create: (order) => Order.create(order),
  count: () => Order.count().lean().exec(),
  getByName: (name) => Order.find({ name: name }).lean().exec(),
};
