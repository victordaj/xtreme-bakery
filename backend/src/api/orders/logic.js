const database = require("./databse");

module.exports = {
  getAll: (options) => {
    return Promise.all([database.getAll(options), database.count()]).then(
      ([orders, count]) => ({ orders, count })
    );
  },
  getOne: (id) => database.getOne(id),
  create: (orders) => database.create(orders),
  edit: (id, orders) => database.update(id, orders),
  delete: (id) => database.delete(id),
  getByName: (name) => database.getByName(name),
};
