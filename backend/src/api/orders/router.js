var express = require("express");
var router = express.Router();
const wrap = require("express-async-wrap");
const logic = require("./logic");

router
  .route("/")
  .post((req, res) => {
    logic.create(req.body.order).then((orders) => {
      res.json(orders);
    });
  })
  .get((req, res) => {
    logic
      .getAll({
        from: Number(req.query.from || 0),
        limit: Number(req.query.limit || 50),
        search: req.query.search || undefined,
        filter: req.query.filter || undefined,
      })
      .then((response) => {
        res.json(response);
      });
  });
router
  .route("/:ID")
  .put((req, res) => {
    logic.edit(req.params.ID, req.body.order).then((orders) => {
      res.json(orders);
    });
  })
  .get((req, res) => {
    logic.getOne(req.params.ID).then((orders) => {
      res.json(orders);
    });
  })
  .delete((req, res) => {
    logic.delete(req.params.ID).then((response) => {
      res.send(response);
    });
  });

router.route("/name/:NAME").put((req, res) => {
  logic.getByName(req.params.NAME).then((order) => {
    res.json(order);
  });
});
module.exports = router;
