const { Product } = require("../../database/models");

module.exports = {
  getAll: (options) => {
    let query = {};
    if (options && options.search && options.search.length) {
      query.name = new RegExp(options.search, "i");
    }
    return Product.find({ ...query })
      .skip(options.from)
      .limit(options.limit)
      .lean()
      .exec();
  },
  getOne: (id) => Product.findById(id).lean().exec(),
  update: (id, product) => Product.findByIpAndUpdate(id, product).lean().exec(),
  delete: (id) => Product.findByIdAndDelete(id),
  create: (product) => Product.create(product),
  count: () => Product.count().lean().exec(),
  getByName: (name) => Product.find({ name: name }).lean().exec(),
};
