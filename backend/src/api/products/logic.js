const database = require("./databse");

module.exports = {
  getAll: (options) => {
    return Promise.all([database.getAll(options), database.count()]).then(
      ([products, count]) => ({ products, count })
    );
  },
  getOne: (id) => database.getOne(id),
  create: (ptoduct) => database.create(ptoduct),
  edit: (id, ptoduct) => database.update(id, ptoduct),
  delete: (id) => database.delete(id),
  getByName: (name) => database.getByName(name),
};
