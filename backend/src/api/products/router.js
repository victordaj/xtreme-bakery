var express = require("express");
var router = express.Router();
const wrap = require("express-async-wrap");
const logic = require("./logic");

router.route("/name/:NAME").get((req, res) => {
  logic.getByName(req.params.NAME).then((product) => {
    res.json(product);
  });
});

router
  .route("/")
  .post((req, res) => {
    logic.create(req.body.product).then((products) => {
      res.json(products);
    });
  })
  .get((req, res) => {
    logic
      .getAll({
        from: Number(req.query.from || 0),
        limit: Number(req.query.limit || 50),
        search: req.query.search || undefined,
      })
      .then((response) => {
        res.json(response);
      });
  });
router
  .route("/:ID")
  .put((req, res) => {
    logic.edit(req.params.ID, req.body.product).then((products) => {
      res.json(products);
    });
  })
  .get((req, res) => {
    logic.getOne(req.params.ID).then((products) => {
      res.send(products);
    });
  })
  .delete((req, res) => {
    logic.delete(req.params.ID).then((response) => {
      res.send(response);
    });
  });

module.exports = router;
