module.exports = {
    User: require('./users'),
    Order: require('./orders'),
    Product: require('./product')
}