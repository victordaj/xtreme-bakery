const mongoose = require('mongoose')
const CONSTANTS = require('../../utils/constants')

const { Schema } = mongoose

const ordersSchema = Schema({
    name: String,
    surname: String,
    email: String,
    phone: String,
    address: String,
    extraInfo: String,
    status: {
        type: String,
        enum: Object.values(CONSTANTS.DATABASE.ORDER_STATUSES),
        default: CONSTANTS.DATABASE.ORDER_STATUSES.PROCESSING,
        index: true
    },
    products: [{
        quantity: Number,
        product:{
            type: Schema.Types.ObjectId,
            ref: CONSTANTS.DATABASE.COLLECTIONS.PRODUCTS
        }
    }]
})
const Order = mongoose.model(CONSTANTS.DATABASE.COLLECTIONS.ORDERS, ordersSchema)

module.exports = Order
