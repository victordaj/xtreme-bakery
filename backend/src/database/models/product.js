const mongoose = require('mongoose')
const CONSTANTS = require('../../utils/constants')

const productsSchema = mongoose.Schema({
    name: String,
    description: String,
    price: Number,
    stock : Number,
    productWeight : Number,
    isDeleted : Boolean,
})
const Product = mongoose.model(CONSTANTS.DATABASE.COLLECTIONS.PRODUCTS, productsSchema)

module.exports = Product
