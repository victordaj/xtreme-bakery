module.exports = {
  PORT: 9000,
  DATABASE: {
    ORDER_STATUSES: {
      NEW: "nou",
      PROCESSING: "procesare",
      TRANSPORTING: "livrare",
      DELIVERed: "livrat",
    },
    ROLES: {
      WORKER: "angajat",
      DELIVERY: "livrator",
      ADMIN: "admin",
    },
    COLLECTIONS: {
      USERS: "users",
      PRODUCTS: "products",
      ORDERS: "orders",
    },
    URL: "mongodb://localhost/mongodb_bakery",
  },
  ORIGIN: "http://localhost:3000",
  SALT_ROUNDS: 10,
  INVALID_PASSWORD: "Invalid password",
  INVALID_EMAIL: "Invalid email",
  JWT: {
    SECRET: "ad78312njk&^#nkb()(djlk1256~!$#&^(*)32",
  },
};
