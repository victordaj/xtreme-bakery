import React from 'react';

import PageHeader from '../common/PageHeader/PageHader';
import TabsMenu from '../common/TabsMenu/TabsMenu';
import Users from './Users/User';
import Products from './Products/Product';
import Orders from './Orders/Orders';
import { UserAdmin32 } from '@carbon/icons-react';

const AppHeader = () => {
  return (
    <div>
      <PageHeader title={'Admin'} icon={<UserAdmin32 />} />
      <TabsMenu
        renderOnClick
        tabs={[
          {
            label: 'Utilizatori',
            component: <Users />,
          },
          {
            label: 'Comenzii',
            component: <Orders />,
          },
          {
            label: 'Produse',
            component: <Products />,
          },
        ]}
      />
    </div>
  );
};

export default AppHeader;
