import React, { useState, useEffect } from 'react';
import { Dropdown, TextInput, NumberInput } from 'carbon-components-react';
import Modal from '../../common/Modal/Modal';
import { useOrder } from '../../../hooks/index';
import { API } from '../../../helpers/index';

const initialState = {
  name: '',
  surname: '',
  phone: '',
  email: '',
  address: '',
  extraInfo: '',
  status: '',
  products: [],
};
const initialProduct = {
  product: '',
  quantity: 0,
};

const OrderModal = (props) => {
  const [fields, setFields] = useState(initialState);
  const [product, setProduct] = useState(initialProduct);

  useEffect(() => {
    API.get(`/orders/${props.id}`).then((data) => {
      setFields({
        name: data.name,
        surname: data.surname,
        phone: data.phone,
        email: data.email,
        address: data.address,
        extraInfo: data.extraInfo,
        status: data.status,
      });
    });
  }, [props.id]);

  const onChange = (event) => {
    setFields({ ...fields, [event.target.name]: event.target.value });
  };

  const onChangeProduct = (event) => {
    setProduct({ ...product, [event.target.name]: event.target.value });
  };

  return (
    <Modal
      open={props.open}
      title={props.title}
      acceptText={props.acceptText}
      cancelText={props.cancelText}
      onSubmit={() => {
        if (props.edit) props.onSubmit(fields);
        if (props.add) props.onSubmit(product);
      }}
      onRequestClose={props.onRequestClose}
      onSecondarySubmit={props.onSecondarySubmit}
    >
      {props.edit && (
        <div>
          <TextInput
            name="name"
            labelText="Nume"
            onChange={onChange}
            value={fields.name}
            light
          ></TextInput>
          <TextInput
            name="surname"
            labelText="Prenume"
            onChange={onChange}
            value={fields.surname}
            light
          ></TextInput>
          <TextInput
            name="phone"
            labelText="Telefon"
            onChange={onChange}
            value={fields.phone}
            light
          ></TextInput>
          <TextInput
            name="role"
            labelText="Rol"
            onChange={onChange}
            value={fields.role}
            light
          ></TextInput>
          <TextInput
            name="email"
            labelText="Email"
            onChange={onChange}
            value={fields.email}
            light
          ></TextInput>
          <TextInput
            name="address"
            labelText="Adresa"
            onChange={onChange}
            value={fields.address}
            light
          ></TextInput>
          <TextInput
            name="extraInfo"
            labelText="Observatii"
            onChange={onChange}
            value={fields.extraInfo}
            light
          ></TextInput>
          <Dropdown
            items={['nou', 'procesare', 'livrare', 'livrat']}
            label="Status"
            selectedItem={fields.status}
            titleText="Status"
            onChange={(event) => {
              onChange({
                target: {
                  name: 'status',
                  value: event.selectedItem,
                },
              });
            }}
            light
          />
        </div>
      )}
      {props.add && (
        <div>
          <Dropdown
            items={props.products || []}
            label="produs"
            titleText="produs"
            onChange={(event) => {
              onChangeProduct({
                target: {
                  name: 'product',
                  value: event.selectedItem,
                },
              });
            }}
            light
          ></Dropdown>
          <NumberInput
            id="tj-input"
            invalidText="Number is not valid"
            label="Cantitate"
            name="quantity"
            onChange={onChangeProduct}
            max={100}
            min={0}
            step={1}
            value={product.quantity}
          />
        </div>
      )}
    </Modal>
  );
};

export default OrderModal;
