import React from 'react';
import Notifications from '../../common/Notification/Notification';

const OrderNotifications = (props) => {
  return (
    <div className="toast-container">
      {props.create.isSuccess && (
        <Notifications
          type="toast"
          kind="success"
          title="Comanda creeata cu succes"
          subtitle="succesfully"
          timeout={5000}
        />
      )}
      {props.create.isError && (
        <Notifications
          type="toast"
          kind="error"
          title="Nu s-a putut creea comanda"
          timeout={5000}
        />
      )}
      {props.edit.isSuccess && (
        <Notifications
          type="toast"
          kind="success"
          title="Comanda editat cu succes"
          subtitle="succesfully"
          timeout={5000}
        />
      )}
      {props.edit.isError && (
        <Notifications
          type="toast"
          kind="error"
          title="Nu s-a putut edita comanda"
          timeout={5000}
        />
      )}
      {props.delete.isSuccess && (
        <Notifications
          type="toast"
          kind="success"
          title="Comanda stearsa cu succes"
          subtitle="succesfully"
          timeout={5000}
        />
      )}
      {props.delete.isError && (
        <Notifications
          type="toast"
          kind="error"
          title="Nu s-a putut sterge comanda"
          timeout={5000}
        />
      )}
    </div>
  );
};

export default OrderNotifications;
