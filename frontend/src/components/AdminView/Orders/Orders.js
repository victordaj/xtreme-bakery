import React, { useEffect, useState } from 'react';
import {
  Button,
  TextInput,
  Pagination,
  Dropdown,
  TextArea,
  ContentSwitcher,
  Switch,
  StructuredListBody,
  StructuredListHead,
  StructuredListWrapper,
  StructuredListCell,
  StructuredListRow,
} from 'carbon-components-react';
import { Edit20, Delete20, TrashCan20 } from '@carbon/icons-react';
import Notifications from './OrderNotification';
import { API } from '../../../helpers/index';
import validator from 'validator';
import { toast } from '../../../helpers/index';

import {
  useCreateOrder,
  useDeleteOrder,
  useEditOrder,
  useOrders,
  useProducts,
} from '../../../hooks/index';

import Table from '../../common/Table/Table';
import Modal from './OrderModal';

import styles from './orders.module.scss';

const initialState = {
  name: { value: '', error: false },
  surname: { value: '', error: false },
  phone: { value: '', error: false },
  email: { value: '', error: false },
  address: { value: '', error: false },
  extraInfo: { value: '', error: false },
  status: 'new',
  products: [],
};

const headers = [
  { content: 'Nume' },
  { content: 'Prenume' },
  { content: 'Telefon' },
  { content: 'E-mail' },
  { content: 'Adresa' },
  { content: 'Observatii' },
  { content: 'Status' },
  { content: 'Actiunii' },
];

const listHeaders = ['Nume', 'Cantitate', 'Actiunii'];

const Users = (props) => {
  const [fields, setFields] = useState(initialState);
  const [openEditOrder, setOpenEditOrder] = useState(false);
  const [openAddProduct, setOpenAddProduct] = useState(false);
  const [page, setPage] = useState(0);
  const [id, setId] = useState('');
  const [filter, setFilter] = useState('nou');
  const [showProducts, setShowProducts] = useState(false);
  const createOrder = useCreateOrder();
  const editOrder = useEditOrder();
  const deleteOrder = useDeleteOrder();
  const products = useProducts({ from: 0, limit: 500 });

  const getOrders = useOrders({ from: 5 * page, limit: 5, filter: filter });
  useEffect(() => {
    ordersRefetch();
  }, [filter]);

  const populateProducts = () => {
    if (products.isSuccess) {
      return products.data.products.map((product) => product.name);
    }
  };

  const addProduct = (products) => {
    API.get(`/products/name/${products.product}`).then((product) => {
      fields.products = [
        ...fields.products,
        { product: product[0], quantity: products.quantity },
      ];
    });
    setOpenAddProduct(false);
    setShowProducts(true);
  };

  const populateTable = () =>
    getOrders.data.orders.map((order) => ({
      id: order._id,
      cells: [
        { value: order.name ? order.name : 'nume', id: order.name },
        { value: order.surname, id: order.surname },
        { value: order.phone, id: order.phone },
        { value: order.email, id: order.email },
        { value: order.address, id: order.address },
        { value: order.extraInfo, id: order.extraInfo },
        { value: order.status, id: order.status },
      ],
    }));

  const ordersRefetch = async () => {
    await getOrders.refetch();
    return null;
  };

  const onChange = (event) => {
    if (event.target.value && event.target.value.length) {
      let err = false;
      if (event.target.name === 'email')
        if (!validator.isEmail(event.target.value)) err = true;
      setFields({
        ...fields,
        [event.target.name]: { value: event.target.value, error: err },
      });
    } else
      setFields({
        ...fields,
        [event.target.name]: { value: event.target.value, error: true },
      });
  };

  const applyEditMutation = (fields) => {
    let body = {};
    Object.keys(fields).forEach((key) => {
      if (fields[key] && fields[key].length) body[key] = fields[key];
    });
    editOrder.mutate(
      { id: id, body: body },
      {
        onSuccess: ordersRefetch,
      }
    );
    setOpenEditOrder(false);
  };

  const applyMutation = () => {
    let ok = true;
    if (
      validator.isEmpty(fields.name.value) &&
      validator.isEmpty(fields.surname.value) &&
      validator.isEmpty(fields.phone.value) &&
      validator.isEmpty(fields.email.value) &&
      validator.isEmpty(fields.address.value) &&
      validator.isEmpty(fields.extraInfo.value)
    ) {
      toast.error('Toate datele trebuie completate');
      ok = false;
    }
    if (!validator.isEmail(fields.email.value)) {
      toast.error('Adresa de email invalida');
      ok = false;
    }
    if (fields.products.length === 0) {
      toast.error('Pentru a pune o comanda trebuie sa adaugati produse');
      ok = false;
    }
    if (ok) {
      const fieldsCopy = {};
      Object.keys(fields).forEach((key) => {
        fieldsCopy[key] = fields[key].value;
      });
      fieldsCopy.products = fields.products;
      createOrder.mutate(fieldsCopy, {
        onSuccess: ordersRefetch,
      });
      setFields(initialState);
    }
  };

  const getPagination = () => {
    if (getOrders.isSuccess) {
      return getOrders.data.count;
    }
    return 0;
  };
  return (
    <div className={`${styles['root-container']} bx--grid`}>
      <div className="bx--row">
        <div className="bx--col-md-6 bx--col-lg-6">
          <div className={`${styles['form-container']}`}>
            <span className={`${styles['title-container']}`}>
              Creeaza comanda
            </span>
            <TextInput
              name="name"
              labelText="Nume"
              onChange={onChange}
              value={fields.name.value}
              invalid={fields.name.error}
              invalidText="Va rugam verificati inca o data"
              light
            ></TextInput>
            <TextInput
              name="surname"
              labelText="Prenume"
              onChange={onChange}
              value={fields.surname.value}
              invalid={fields.surname.error}
              invalidText="Va rugam verificati inca o data"
              light
            ></TextInput>
            <TextInput
              name="phone"
              labelText="Telefon"
              onChange={onChange}
              value={fields.phone.value}
              invalid={fields.phone.error}
              invalidText="Va rugam verificati inca o data"
              light
            ></TextInput>
            <TextInput
              name="email"
              labelText="Email"
              onChange={onChange}
              value={fields.email.value}
              invalid={fields.email.error}
              invalidText="Va rugam verificati inca o data"
              light
            ></TextInput>
            <TextInput
              name="address"
              labelText="Adresa"
              onChange={onChange}
              value={fields.address.value}
              invalid={fields.address.error}
              invalidText="Va rugam verificati inca o data"
              light
            ></TextInput>
            <TextArea
              name="extraInfo"
              labelText="Observatii"
              onChange={onChange}
              value={fields.extraInfo.value}
              invalid={fields.extraInfo.error}
              invalidText="Va rugam verificati inca o data"
              light
            ></TextArea>
            <Dropdown
              items={['nou', 'procesare', 'livrare', 'livrat']}
              label="Status"
              titleText="Status"
              onChange={(event) => {
                onChange({
                  target: {
                    name: 'status',
                    value: event.selectedItem,
                  },
                });
              }}
              light
            />
            {showProducts && fields.products.length && (
              <div className={styles.list}>
                <StructuredListWrapper ariaLabel="Structured list">
                  <StructuredListHead>
                    <StructuredListRow head tabIndex={0}>
                      {listHeaders.map((header) => (
                        <StructuredListCell head>{header}</StructuredListCell>
                      ))}
                    </StructuredListRow>
                  </StructuredListHead>
                  <StructuredListBody>
                    {fields.products.map((product) => (
                      <StructuredListRow tabIndex={0}>
                        <StructuredListCell>
                          {product.product.name}
                        </StructuredListCell>
                        <StructuredListCell>
                          {product.quantity}
                        </StructuredListCell>
                        <StructuredListCell>
                          <TrashCan20
                            className={styles.delete}
                            onClick={() => {
                              let fieldsCopy = fields.products.filter(
                                (p) => p.quantity !== product.quantity
                              );
                              setFields({ ...fields, products: fieldsCopy });
                            }}
                          />
                        </StructuredListCell>
                      </StructuredListRow>
                    ))}
                  </StructuredListBody>
                </StructuredListWrapper>
              </div>
            )}
            <div className={`${styles['button-container']}`}>
              <Button
                kind="danger"
                size="field"
                onClick={() => setOpenAddProduct(true)}
              >
                Adauga Produs
              </Button>
              <Button
                kind="secondary"
                size="field"
                onClick={() => applyMutation()}
              >
                Creeaza
              </Button>
            </div>
          </div>
        </div>
        <div className="bx--col-md-10 bx--col-lg-10">
          <div className={`${styles['content-switcher']}`}>
            <ContentSwitcher
              size="md"
              onChange={async (event) => {
                setFilter(event.name);
              }}
            >
              <Switch name={'nou'} text="Comenzii noi" />
              <Switch name={'procesare'} text="Comenzii in procesare" />
              <Switch name={'livrare'} text="Comenzii in livrare" />
              <Switch name={'livrate'} text="Comenzii livrate" />
            </ContentSwitcher>
          </div>
          {getOrders.isSuccess && (
            <Table
              title="Utilizatori"
              headers={headers}
              rows={populateTable()}
              useZebraStyle={true}
              actions={[
                {
                  tooltip: {
                    text: 'edit',
                    props: {
                      direction: 'top',
                    },
                  },
                  icon: <Edit20 className={`${styles['icons-container']}`} />,
                  onClick: (id) => {
                    setId(id);
                    setOpenEditOrder(true);
                  },
                },
                {
                  tooltip: {
                    text: 'delete',
                    props: {
                      direction: 'top',
                    },
                  },
                  icon: <Delete20 className={`${styles['icons-container']}`} />,
                  onClick: (id) => {
                    if (
                      window.confirm(
                        'Sigur doritit sa stergeti acest utilizator ?'
                      )
                    )
                      deleteOrder.mutate(id, {
                        onSuccess: ordersRefetch,
                      });
                  },
                },
              ]}
            />
          )}
          <Pagination
            backwardText="Previous page"
            forwardText="Next page"
            itemsPerPageText="Items per page:"
            // page={page}
            pageNumberText="Page Number"
            pageSize={5}
            pageSizes={[5]}
            onChange={(event) => {
              setPage(event.page);
              ordersRefetch();
            }}
            totalItems={getPagination()}
          />
        </div>
      </div>

      <Modal
        edit
        id={id}
        open={openEditOrder}
        onSubmit={applyEditMutation}
        onRequestClose={() => setOpenEditOrder(false)}
        onSecondarySubmit={() => setOpenEditOrder(false)}
        title="Editare comanda"
        subtitle="Die bereits getätigten Einstellungen bleiben erhalten."
        cancelText="Inchide"
        acceptText="Editeaza"
      />
      {openAddProduct && (
        <Modal
          add
          products={populateProducts()}
          open={openAddProduct}
          onSubmit={addProduct}
          onRequestClose={() => setOpenAddProduct(false)}
          onSecondarySubmit={() => setOpenAddProduct(false)}
          title="Adauga Produs"
          subtitle="Die bereits getätigten Einstellungen bleiben erhalten."
          cancelText="Inchide"
          acceptText="Adauga"
        />
      )}
      <Notifications
        create={createOrder}
        edit={editOrder}
        delete={deleteOrder}
      />
    </div>
  );
};

export default Users;
