import React, { useState } from 'react';
import { Button, TextInput, Pagination } from 'carbon-components-react';
import { Edit20, Delete20 } from '@carbon/icons-react';
import Notifications from './ProductNotification';
import validator from 'validator';
import { toast } from '../../../helpers/index';

import {
  useCreateProduct,
  useDeleteProduct,
  useEditProduct,
  useProducts,
} from '../../../hooks/index';

import Table from '../../common/Table/Table';
import Modal from './ProductModal';

import styles from './products.module.scss';

const initialState = {
  name: { value: '', error: false },
  description: { value: '', error: false },
  price: { value: '', error: false },
  stock: { value: '', error: false },
  productWeight: { value: '', error: false },
};

const headers = [
  { content: 'Nume' },
  { content: 'Descriere' },
  { content: 'Pret' },
  { content: 'Stoc' },
  { content: 'Greutate' },
  { content: 'Actiunii' },
];

const Products = (props) => {
  const [fields, setFields] = useState(initialState);
  const [openEditProduct, setOpenEditProduct] = useState(false);
  const [page, setPage] = useState(0);
  const [id, setId] = useState('');
  const createProduct = useCreateProduct();
  const editProduct = useEditProduct();
  const deleteProduct = useDeleteProduct();

  const getProducts = useProducts({ from: 5 * page, limit: 5 });

  const populateTable = () =>
    getProducts.data.products.map((user) => ({
      id: user._id,
      cells: [
        { value: user.name ? user.name : 'nume', id: user.name },
        { value: user.description, id: user.description },
        { value: user.price, id: user.price },
        { value: user.stock, id: user.stock },
        { value: user.productWeight, id: user.productWeight },
      ],
    }));

  const productRefetch = async () => {
    await getProducts.refetch();
    return null;
  };

  const onChange = (event) => {
    if (event.target.value && event.target.value.length) {
      let err = false;
      if (
        event.target.name === 'price' ||
        event.target.name === 'stock' ||
        event.target.name === 'productWeight'
      )
        if (!validator.isNumeric(event.target.value)) err = true;
      setFields({
        ...fields,
        [event.target.name]: { value: event.target.value, error: err },
      });
    } else
      setFields({
        ...fields,
        [event.target.name]: { value: event.target.value, error: true },
      });
  };

  const applyEditMutation = (fields) => {
    let body = {};
    Object.keys(fields).forEach((key) => {
      if (fields[key] && fields[key].length) body[key] = fields[key];
    });
    editProduct.mutate(
      { id: id, body: body },
      {
        onSuccess: productRefetch,
      }
    );
    setOpenEditProduct(false);
  };

  const applyMutation = () => {
    let ok = true;
    if (
      validator.isEmpty(fields.name.value) &&
      validator.isEmpty(fields.description.value) &&
      validator.isEmpty(fields.price.value) &&
      validator.isEmpty(fields.stock.value) &&
      validator.isEmpty(fields.productWeight.value)
    ) {
      toast.error('Toate datele trebuie completate');
      ok = false;
    }
    if (
      !validator.isNumeric(fields.price.value) &&
      !validator.isNumeric(fields.stock.value) &&
      !validator.isNumeric(fields.productWeight.value)
    ) {
      toast.error('Pret/Stoc/Greutate trebuie sa fie valori numerice');
      ok = false;
    }
    if (ok) {
      const fieldsCopy = {};
      Object.keys(fields).forEach((key) => {
        fieldsCopy[key] = fields[key].value;
      });
      createProduct.mutate(fieldsCopy, {
        onSuccess: productRefetch,
      });
      setFields(initialState);
    }
  };

  const getPagination = () => {
    if (getProducts.isSuccess) {
      return getProducts.data.count;
    }
    return 0;
  };
  return (
    <div className={`${styles['root-container']} bx--grid`}>
      <div className="bx--row">
        <div className="bx--col-md-6 bx--col-lg-6">
          <div className={`${styles['form-container']}`}>
            <span className={`${styles['title-container']}`}>
              Creeaza produs
            </span>
            <TextInput
              name="name"
              labelText="Nume"
              onChange={onChange}
              value={fields.name.value}
              invalid={fields.name.error}
              invalidText="Va rugam verificati inca o data"
              light
            ></TextInput>
            <TextInput
              name="description"
              labelText="Descriere"
              onChange={onChange}
              value={fields.description.value}
              invalid={fields.description.error}
              invalidText="Va rugam verificati inca o data"
              light
            ></TextInput>
            <TextInput
              name="price"
              labelText="Pret"
              onChange={onChange}
              value={fields.price.value}
              invalid={fields.price.error}
              invalidText="Va rugam verificati inca o data"
              light
            ></TextInput>
            <TextInput
              name="productWeight"
              labelText="Greutate"
              onChange={onChange}
              value={fields.productWeight.value}
              invalid={fields.productWeight.error}
              invalidText="Va rugam verificati inca o data"
              light
              helperText="gr"
            ></TextInput>
            <TextInput
              name="stock"
              labelText="Stoc"
              onChange={onChange}
              value={fields.stock.value}
              invalid={fields.stock.error}
              invalidText="Va rugam verificati inca o data"
              helperText="RON"
              light
            ></TextInput>
            <div className={`${styles['button-container']}`}>
              <Button
                kind="secondary"
                size="field"
                onClick={() => applyMutation()}
              >
                Creeaza
              </Button>
            </div>
          </div>
        </div>
        <div className="bx--col-md-10 bx--col-lg-10">
          {getProducts.isSuccess && (
            <Table
              title="Produse"
              headers={headers}
              rows={populateTable()}
              useZebraStyle={true}
              actions={[
                {
                  tooltip: {
                    text: 'edit',
                    props: {
                      direction: 'top',
                    },
                  },
                  icon: <Edit20 className={`${styles['icons-container']}`} />,
                  onClick: (id) => {
                    setId(id);
                    setOpenEditProduct(true);
                  },
                },
                {
                  tooltip: {
                    text: 'delete',
                    props: {
                      direction: 'top',
                    },
                  },
                  icon: <Delete20 />,
                  onClick: (id) => {

                    if (
                      window.confirm(
                        'Sigur doritit sa stergeti acest utilizator ?'
                      )
                    )
                      deleteProduct.mutate(id, {
                        onSuccess: productRefetch,
                      });
                  },
                },
              ]}
            />
          )}
          <Pagination
            backwardText="Previous page"
            forwardText="Next page"
            itemsPerPageText="Items per page:"
            page={page}
            pageNumberText="Page Number"
            pageSize={5}
            pageSizes={[5]}
            onChange={(event) => {
              setPage(event.page);
              productRefetch();
            }}
            totalItems={getPagination()}
          />
        </div>
      </div>
      {openEditProduct && (
        <Modal
          open={openEditProduct}
          id={id}
          onSubmit={applyEditMutation}
          onRequestClose={() => setOpenEditProduct(false)}
          onSecondarySubmit={() => setOpenEditProduct(false)}
          title="Editare utilizator"
          subtitle="Die bereits getätigten Einstellungen bleiben erhalten."
          cancelText="Inchide"
          acceptText="Editeaza"
          type="edit"
        />
      )}
      <Notifications
        create={createProduct}
        edit={editProduct}
        delete={deleteProduct}
      />
    </div>
  );
};

export default Products;
