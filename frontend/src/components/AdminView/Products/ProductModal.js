import React, { useEffect, useState } from 'react';
import { TextInput } from 'carbon-components-react';
import Modal from '../../common/Modal/Modal';
import { API } from '../../../helpers/index';

const initialState = {
  name: '',
  description: '',
  price: '',
  stock: '',
  productWeight: '',
};

const ProductModal = (props) => {
  const [fields, setFields] = useState(initialState);

  useEffect(() => {
    API.get(`products/${props.id}`).then((product) =>
      setFields({
        name: product.name,
        description: product.description,
        price: product.price,
        stock: product.stock,
        productWeight: product.productWeight,
      })
    );
  }, [props.id]);
  const onChange = (event) => {
    setFields({ ...fields, [event.target.name]: event.target.value });
  };

  return (
    <Modal
      open={props.open}
      title={props.title}
      acceptText={props.acceptText}
      cancelText={props.cancelText}
      onSubmit={() => props.onSubmit(fields)}
      onRequestClose={props.onRequestClose}
      onSecondarySubmit={props.onSecondarySubmit}
    >
      <TextInput
        name="name"
        labelText="Nume"
        onChange={onChange}
        value={fields.name}
        light
      ></TextInput>
      <TextInput
        name="description"
        labelText="Descriere"
        onChange={onChange}
        value={fields.description}
        light
      ></TextInput>
      <TextInput
        name="price"
        labelText="Pret"
        onChange={onChange}
        value={fields.price}
        light
      ></TextInput>
      <TextInput
        name="productWeight"
        labelText="Greutate"
        onChange={onChange}
        value={fields.productWeight}
        light
      ></TextInput>
      <TextInput
        name="stock"
        labelText="Stoc"
        onChange={onChange}
        value={fields.stock}
        light
      ></TextInput>
    </Modal>
  );
};

export default ProductModal;
