import React from 'react';
import Notifications from '../../common/Notification/Notification';

const ProductNotifications = (props) => {
  return (
    <div className="toast-container">
      {props.create.isSuccess && (
        <Notifications
          type="toast"
          kind="success"
          title="Produs creat cu succes"
          subtitle="succesfully"
          timeout={5000}
        />
      )}
      {props.create.isError && (
        <Notifications
          type="toast"
          kind="error"
          title="Nu s-a putut creea produsul"
          timeout={5000}
        />
      )}
      {props.edit.isSuccess && (
        <Notifications
          type="toast"
          kind="success"
          title="Produs editat cu succes"
          subtitle="succesfully"
          timeout={5000}
        />
      )}
      {props.edit.isError && (
        <Notifications
          type="toast"
          kind="error"
          title="Nu s-a putut edita produsul"
          timeout={5000}
        />
      )}
      {props.delete.isSuccess && (
        <Notifications
          type="toast"
          kind="success"
          title="Produs sters cu succes"
          subtitle="succesfully"
          timeout={5000}
        />
      )}
      {props.delete.isError && (
        <Notifications
          type="toast"
          kind="error"
          title="Nu s-a putut sterge produsul"
          timeout={5000}
        />
      )}
    </div>
  );
};

export default ProductNotifications;
