import React, { useState } from 'react';
import { Button, TextInput, Pagination } from 'carbon-components-react';
import { Edit20, Delete20 } from '@carbon/icons-react';
import Notifications from './UserNotification';
import validator from 'validator';
import { toast } from '../../../helpers/index';
import {
  useCreateUser,
  useDeleteUser,
  useEditUser,
  useUsers,
} from '../../../hooks/index';

import Table from '../../common/Table/Table';
import Modal from './UserModal';

import styles from './user.module.scss';

const initialState = {
  name: { value: '', error: false },
  surname: { value: '', error: false },
  password: { value: '', error: false },
  phone: { value: '', error: false },
  email: { value: '', error: false },
  role: { value: '', error: false },
};

const headers = [
  { content: 'Nume' },
  { content: 'Prenume' },
  { content: 'Telefon' },
  { content: 'E-mail' },
  { content: 'Rol' },
  { content: 'Actiuni' },
];

const Users = (props) => {
  const [fields, setFields] = useState(initialState);
  const [openEditUser, setOpenEditUser] = useState(false);
  const [page, setPage] = useState(0);
  const [id, setId] = useState('');
  const createUser = useCreateUser();
  const editUser = useEditUser();
  const deleteUser = useDeleteUser();

  const getUsers = useUsers({ from: 5 * page, limit: 5 });

  const populateTable = () =>
    getUsers.data.users.map((user) => ({
      id: user._id,
      cells: [
        { value: user.name ? user.name : 'nume', id: user.name },
        { value: user.surname, id: user.surname },
        { value: user.phone, id: user.phone },
        { value: user.email, id: user.email },
        { value: user.role, id: user.role },
      ],
    }));

  const usersRefetch = async () => {
    await getUsers.refetch();
    return null;
  };

  const onChange = (event) => {
    if (event.target.value && event.target.value.length) {
      let err = false;
      if (event.target.name === 'email')
        if (!validator.isEmail(event.target.value)) err = true;
      if (event.target.name === 'phone')
        if (!validator.isMobilePhone(event.target.value)) err = true;
      setFields({
        ...fields,
        [event.target.name]: { value: event.target.value, error: err },
      });
    } else
      setFields({
        ...fields,
        [event.target.name]: { value: event.target.value, error: true },
      });
  };

  const applyEditMutation = (fields) => {
    let body = {};
    Object.keys(fields).forEach((key) => {
      if (fields[key] && fields[key].length) body[key] = fields[key];
    });
    editUser.mutate(
      { id: id, body: body },
      {
        onSuccess: usersRefetch,
      }
    );
    setOpenEditUser(false);
  };

  const applyMutation = () => {
    let ok = true;
    if (
      validator.isEmpty(fields.name.value) ||
      validator.isEmpty(fields.surname.value) ||
      validator.isEmpty(fields.email.value) ||
      validator.isEmpty(fields.password.value) ||
      validator.isEmpty(fields.role.value)
    ) {
      toast.error('Toate datele trebuie completate');
      ok = false;
    }
    if (!validator.isEmail(fields.email.value)) {
      toast.error('Adresa de email invalida');
      ok = false;
    }

    if (ok) {
      const fieldsCopy = {};
      Object.keys(fields).forEach((key) => {
        fieldsCopy[key] = fields[key].value;
      });
      createUser.mutate(fieldsCopy, {
        onSuccess: usersRefetch,
      });
      setFields(initialState);
    }
  };
  const getPagination = () => {
    if (getUsers.isSuccess) {
      return getUsers.data.count;
    }
    return 0;
  };
  return (
    <div className={`${styles['root-container']} bx--grid`}>
      <div className="bx--row">
        <div className="bx--col-md-6 bx--col-lg-6">
          <div className={`${styles['form-container']}`}>
            <span className={`${styles['title-container']}`}>
              Creeaza utilizator
            </span>
            <TextInput
              name="name"
              labelText="Nume"
              invalid={fields.name.error}
              invalidText="Va rugam verificati inca o data"
              onChange={onChange}
              value={fields.name.value}
              light
            ></TextInput>
            <TextInput
              name="surname"
              labelText="Prenume"
              invalid={fields.surname.error}
              invalidText="Va rugam verificati inca o data"
              onChange={onChange}
              value={fields.surname.value}
              light
            ></TextInput>
            <TextInput.PasswordInput
              name="password"
              labelText="Parola"
              invalid={fields.password.error}
              invalidText="Va rugam verificati inca o data"
              onChange={onChange}
              value={fields.password.value}
              light
            ></TextInput.PasswordInput>
            <TextInput
              name="phone"
              labelText="Telefon"
              invalid={fields.phone.error}
              invalidText="Va rugam verificati inca o data"
              onChange={onChange}
              value={fields.phone.value}
              light
            ></TextInput>
            <TextInput
              name="role"
              labelText="Rol"
              invalidText="Va rugam verificati inca o data"
              invalid={fields.role.error}
              onChange={onChange}
              value={fields.role.value}
              light
            ></TextInput>
            <TextInput
              name="email"
              labelText="Email"
              invalid={fields.email.error}
              invalidText="Va rugam verificati inca o data"
              onChange={onChange}
              value={fields.email.value}
              light
            ></TextInput>
            <div className={`${styles['button-container']}`}>
              <Button
                kind="secondary"
                size="field"
                onClick={() => applyMutation()}
              >
                Creeaza
              </Button>
            </div>
          </div>
        </div>
        <div className="bx--col-md-10 bx--col-lg-10">
          {getUsers.isSuccess && (
            <Table
              title="Utilizatori"
              headers={headers}
              rows={populateTable()}
              useZebraStyle={true}
              actions={[
                {
                  tooltip: {
                    text: 'edit',
                    props: {
                      direction: 'top',
                    },
                  },
                  icon: <Edit20 className={`${styles['icons-container']}`} />,
                  onClick: (id) => {
                    setId(id);
                    setOpenEditUser(true);
                  },
                },
                {
                  tooltip: {
                    text: 'delete',
                    props: {
                      direction: 'top',
                    },
                  },
                  icon: <Delete20 />,
                  onClick: (id) => {
                    if (
                      window.confirm(
                        'Sigur doritit sa stergeti acest utilizator ?'
                      )
                    )
                      deleteUser.mutate(id, {
                        onSuccess: usersRefetch,
                      });
                  },
                },
              ]}
            />
          )}
          <Pagination
            backwardText="Previous page"
            forwardText="Next page"
            itemsPerPageText="Items per page:"
            page={page}
            pageNumberText="Page Number"
            pageSize={5}
            pageSizes={[5]}
            onChange={(event) => {
              setPage(event.page);
              usersRefetch();
            }}
            totalItems={getPagination()}
          />
        </div>
      </div>
      {openEditUser && (
        <Modal
          open={openEditUser}
          id={id}
          onSubmit={applyEditMutation}
          onRequestClose={() => setOpenEditUser(false)}
          onSecondarySubmit={() => setOpenEditUser(false)}
          title="Editare utilizator"
          subtitle="Die bereits getätigten Einstellungen bleiben erhalten."
          cancelText="Inchide"
          acceptText="Editeaza"
          type="edit"
        />
      )}
      <Notifications create={createUser} edit={editUser} delete={deleteUser} />
    </div>
  );
};

export default Users;
