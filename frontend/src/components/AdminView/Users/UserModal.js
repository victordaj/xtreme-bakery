import React, { useEffect, useState } from 'react';
import { TextInput } from 'carbon-components-react';
import Modal from '../../common/Modal/Modal';
import { API } from '../../../helpers/index';

const initialState = {
  name: '',
  surname: '',
  password: '',
  phone: '',
  email: '',
  role: '',
};

const Users = (props) => {
  const [fields, setFields] = useState(initialState);

  useEffect(() => {
    API.get(`/users/${props.id}`).then((user) =>
      setFields({
        name: user.name,
        surname: user.surname,
        phone: user.phone,
        email: user.email,
        role: user.role,
      })
    );
  }, [props.id]);

  const onChange = (event) => {
    setFields({ ...fields, [event.target.name]: event.target.value });
  };

  return (
    <Modal
      open={props.open}
      title={props.title}
      acceptText={props.acceptText}
      cancelText={props.cancelText}
      onSubmit={() => props.onSubmit(fields)}
      onRequestClose={props.onRequestClose}
      onSecondarySubmit={props.onSecondarySubmit}
    >
      <TextInput
        name="name"
        labelText="Nume"
        onChange={onChange}
        value={fields.name}
        light
      ></TextInput>
      <TextInput
        name="surname"
        labelText="Prenume"
        onChange={onChange}
        value={fields.surname}
        light
      ></TextInput>
      <TextInput.PasswordInput
        name="password"
        labelText="Parola"
        onChange={onChange}
        value={fields.password}
        light
      ></TextInput.PasswordInput>
      <TextInput
        name="phone"
        labelText="Telefon"
        onChange={onChange}
        value={fields.phone}
        light
      ></TextInput>
      <TextInput
        name="role"
        labelText="Rol"
        onChange={onChange}
        value={fields.role}
        light
      ></TextInput>
      <TextInput
        name="email"
        labelText="Email"
        onChange={onChange}
        value={fields.email}
        light
      ></TextInput>
    </Modal>
  );
};

export default Users;
