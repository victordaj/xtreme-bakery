import React from 'react';
import Notifications from '../../common/Notification/Notification';

const UserNotifications = (props) => {
  return (
    <div className="toast-container">
      {props.create.isSuccess && (
        <Notifications
          type="toast"
          kind="success"
          title="Utilizator creat cu succes"
          subtitle="succesfully"
          timeout={5000}
        />
      )}
      {props.create.isError && (
        <Notifications
          type="toast"
          kind="error"
          title="Nu s-a putut creea uitlizatorul"
          timeout={5000}
        />
      )}
      {props.edit.isSuccess && (
        <Notifications
          type="toast"
          kind="success"
          title="Utilizator editat cu succes"
          subtitle="succesfully"
          timeout={5000}
        />
      )}
      {props.edit.isError && (
        <Notifications
          type="toast"
          kind="error"
          title="Nu s-a putut edita uitlizatorul"
          timeout={5000}
        />
      )}
      {props.delete.isSuccess && (
        <Notifications
          type="toast"
          kind="success"
          title="Utilizator sters cu succes"
          subtitle="succesfully"
          timeout={5000}
        />
      )}
      {props.delete.isError && (
        <Notifications
          type="toast"
          kind="error"
          title="Nu s-a putut sterge uitlizatorul"
          timeout={5000}
        />
      )}
    </div>
  );
};

export default UserNotifications;
