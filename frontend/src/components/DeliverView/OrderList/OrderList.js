import React, { useState } from 'react';

import Table from '../../common/Table/Table';
import { useOrders } from '../../../hooks/index';
import { CheckmarkOutline20 } from '@carbon/icons-react';

import { Pagination } from 'carbon-components-react';
import { useEditOrder } from '../../../hooks/index';

import styles from './order-list.module.scss';
const headers = [
  { content: 'Nume' },
  { content: 'Telefon' },
  { content: 'Adresa' },
  { content: 'Observatii' },
  { content: 'Actiunii' },
];
const List = (props) => {
  const getOrders = useOrders({ from: 0, limit: 500, filter: 'livrare' });
  const switchToDone = useEditOrder();

  const ordersRefetch = async () => {
    await getOrders.refetch();
    return null;
  };

  const populateTable = () => {
    if (
      getOrders.isSuccess &&
      getOrders.data.orders &&
      getOrders.data.orders.length
    ) {
      return getOrders.data.orders.map((order) => ({
        id: order._id,
        cells: [
          { value: order.name ? order.name : 'nume', id: order.name },
          { value: order.phone, id: order.phone },
          { value: order.address, id: order.address },
          { value: order.extraInfo, id: order.extraInfo },
        ],
      }));
    } else return [];
  };
  return (
    <div className={`${styles['root-container']} bx--grid`}>
      <div className="bx--row">
        <div
          className={`${styles['content-container']} bx--col-lg-12 bx--col-md-12`}
        >
          {getOrders.isSuccess && !getOrders.isLoading && !getOrders.isIdle && (
            <Table
              title="Utilizatori"
              headers={headers}
              rows={populateTable()}
              actions={[
                {
                  tooltip: {
                    text: 'Livrat',
                    props: {
                      direction: 'top',
                    },
                  },
                  icon: (
                    <CheckmarkOutline20
                      className={`${styles['icons-container']}`}
                    />
                  ),
                  onClick: (id) => {
                    if (window.confirm('Sigur ati livrat aceasta comanda ?'))
                      switchToDone.mutate(
                        {
                          id,
                          order: { status: 'livrat' },
                        },
                        { onSucces: ordersRefetch }
                      );
                  },
                },
              ]}
            />
          )}
          <Pagination
            backwardText="Previous page"
            forwardText="Next page"
            itemsPerPageText="Items per page:"
            pageNumberText="Page Number"
            pageSize={5}
            pageSizes={[5]}
          />
        </div>
        <div className="bx--col"></div>
      </div>
    </div>
  );
};

export default List;
