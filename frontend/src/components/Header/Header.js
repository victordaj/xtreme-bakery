import {
  Header,
  HeaderName,
  SkipToContent,
  HeaderGlobalAction,
  HeaderGlobalBar,
} from 'carbon-components-react';
import { Logout20 } from '@carbon/icons-react';
import { API } from '../../helpers/index';
import { withRouter } from 'react-router-dom';

const AppHeader = (props) => {
  const logout = () => {
    API.post('/logout').then(() => localStorage.removeItem('token'));
  };
  return (
    <Header aria-label={`${props.prefix} ${props.headerName}`}>
      <SkipToContent />
      <HeaderName prefix={props.prefix}>{props.headerName}</HeaderName>
      {props.children}
      <HeaderGlobalBar>
        <HeaderGlobalAction
          aria-label="App Switcher"
          onClick={() => {
            logout();
            props.history.push('/');
          }}
        >
          <Logout20 />
        </HeaderGlobalAction>
      </HeaderGlobalBar>
    </Header>
  );
};

export default withRouter(AppHeader);
