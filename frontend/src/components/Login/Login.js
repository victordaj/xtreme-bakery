import React, { useState } from 'react';
import './_login.scss';
import { API, toast } from '../../helpers/index';
import { ROLES } from '../../utils/constants';
import { withRouter } from 'react-router-dom';

import logo from '../../files/logo.jpg';

import { Form, TextInput, Button } from 'carbon-components-react';

const Login = (props) => {
  const [fields, setFields] = useState({ email: '', password: '' });

  const onChange = (event) => {
    setFields({ ...fields, [event.target.name]: event.target.value });
  };

  const login = () => {
    API.post('/login', fields).then((response) => {
      if (response.status === 404) {
        toast.error('Utilizator nu a putut fi gasit');
        return;
      }
      if (response.status === 406) {
        toast.error('Parola incorecta');
        return;
      }
      localStorage.setItem('token', response.token);
      localStorage.setItem('userRole', response.user.role);
      toast.error('V-ati logat cu succes');
      switch (response.user.role) {
        case ROLES.ADMIN:
          props.history.push('/admin');
          break;
        case ROLES.DELIVERY:
          props.history.push('/deliver');
          break;
        case ROLES.WORKER:
          props.history.push('/tablet');
          break;
        default:
          break;
      }
      props.refetch();
    });
  };

  return (
    <div className="login-root-container bx--grid ">
      <div>
        <div className="login-image-container bx--col ">
          <img className="login-header-image-container" src={logo} />
        </div>
        <div className="login-form-container bx--col ">
          <div>
            <Form />
          </div>
          <div>
            <TextInput
              id="email"
              labelText="E-mail"
              name="email"
              placeholder="E-mail ..."
              onChange={onChange}
            />
            <TextInput.PasswordInput
              id="password"
              labelText="Parola"
              name="password"
              placeholder="Parola ..."
              onChange={onChange}
            />
          </div>
          <div className="login-button-container bx--col">
            <Button
              className=" bx--col"
              kind="danger"
              onClick={() => login()}
              // renderIcon={}
            >
              Logare
            </Button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default withRouter(Login);
