import React from 'react';

import logo from './images/page-header.png';
import styles from './product-page.module.scss';

const ProductPage = () => {
  return (
    <div className={styles.root}>
      <div className={styles.header}>
        <div className={styles.head}>
          <div></div>
          <span className={styles.title}>Bakery</span>
        </div>
      </div>
    </div>
  );
};

export default ProductPage;
