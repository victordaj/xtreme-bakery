import React, { useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import DeliverView from './DeliverView/DeliverView';
import Login from './Login/Login';
import ProductPage from './ProductPage/ProductPage';
import TabletView from './TabletView/TabletView';
import AdminView from './AdminView/AdminView';
import { API } from '../helpers/index';
import { useIsLogged } from '../hooks/index';

import { QueryClientProvider, QueryClient } from 'react-query';
import Header from './Header/Header';
import Footer from './common/Footer/Footer';

const query = new QueryClient();

const Root = (props) => {
  const [isLogged, setIsLogged] = React.useState(false);
  const [render, setRender] = React.useState(false);
  const [role, setRole] = React.useState('');
  const logged = useIsLogged();

  const isLoggedRefetch = async () => {
    await logged.refetch();
    return null;
  };

  useEffect(() => {
    API.get('/isLogged').then((resp) => {
      setRender(true);
      if (resp._id) {
        setRole(resp.role);
        setIsLogged(true);
      }
    });
  });

  return (
    render && (
      <QueryClientProvider client={query}>
        <Router>
          {isLogged ? (
            <div>
              <Header headerName="Bakery" prefix="" />
              {role === 'angajat' && (
                <Switch>
                  <Route
                    exact
                    path="/"
                    component={(props) => (
                      <Login {...props} refetch={() => isLoggedRefetch()} />
                    )}
                  />
                  <Route exact path="/tablet" component={TabletView} />
                </Switch>
              )}
              {role === 'livrator' && (
                <Switch>
                  <Route
                    exact
                    path="/"
                    component={(props) => (
                      <Login {...props} refetch={() => isLoggedRefetch()} />
                    )}
                  />
                  <Route exact path="/deliver" component={DeliverView} />
                </Switch>
              )}
              {role === 'admin' && (
                <Switch>
                  <Route
                    exact
                    path="/"
                    component={(props) => (
                      <Login {...props} refetch={() => isLoggedRefetch()} />
                    )}
                  />
                  <Route exact path="/tablet" component={TabletView} />
                  <Route exact path="/page" component={ProductPage} />
                  <Route exact path="/deliver" component={DeliverView} />
                  <Route exact path="/admin" component={AdminView} />
                </Switch>
              )}
              <Footer />
            </div>
          ) : (
            <Switch>
              <Route
                exact
                path="/"
                component={(props) => (
                  <Login {...props} refetch={() => isLoggedRefetch()} />
                )}
              />
              <Route
                exact
                path="**"
                component={(props) => (
                  <Login {...props} refetch={() => isLoggedRefetch()} />
                )}
              />
            </Switch>
          )}
        </Router>
      </QueryClientProvider>
    )
  );
};

export default Root;
