import React, { useState } from 'react';

import Table from '../../common/Table/Table';
import { useOrders, useEditOrder } from '../../../hooks/index';
import { DeliveryTruck20, Launch20 } from '@carbon/icons-react';
import Modal from './ProductModal';

import { Pagination } from 'carbon-components-react';

import styles from './order-list.module.scss';
const headers = [
  { content: 'Nume' },
  { content: 'Prenume' },
  { content: 'Telefon' },
  { content: 'E-mail' },
  { content: 'Adresa' },
  { content: 'Observatii' },
  { content: 'Actiunii' },
];
const OrderList = () => {
  const [id, setId] = useState('');
  const [open, setOpen] = useState(false);
  const getOrders = useOrders({ from: 0, limit: 500, filter: 'procesare' });
  const switchToDelivery = useEditOrder();

  const populateTable = () => {
    if (
      getOrders.isSuccess &&
      getOrders.data.orders &&
      getOrders.data.orders.length
    ) {
      return getOrders.data.orders.map((order) => ({
        id: order._id,
        cells: [
          { value: order.name ? order.name : 'nume', id: order.name },
          { value: order.phone, id: order.phone },
          { value: order.address, id: order.address },
          { value: order.email, id: order.email },
          { value: order.address, id: order.address },
          { value: order.extraInfo, id: order.extraInfo },
        ],
      }));
    } else return [];
  };

  const ordersRefetch = async () => {
    await getOrders.refetch();
    return null;
  };

  return (
    <div className={`${styles['root-container']} bx--grid`}>
      <div className="bx--row">
        <div
          className={`${styles['content-container']} bx--col-lg-12 bx--col-md-12`}
        >
          {getOrders.isSuccess && !getOrders.isLoading && !getOrders.isIdle && (
            <Table
              title="Utilizatori"
              headers={headers}
              rows={populateTable()}
              actions={[
                {
                  tooltip: {
                    text: 'Vizualizare produse',
                    props: {
                      direction: 'top',
                    },
                  },
                  icon: <Launch20 className={`${styles['icons-container']}`} />,
                  onClick: (id) => {
                    setId(id);
                    setOpen(true);
                  },
                },
                {
                  tooltip: {
                    text: 'muta in livrare',
                    props: {
                      direction: 'top',
                    },
                  },
                  icon: (
                    <DeliveryTruck20
                      className={`${styles['icons-container']}`}
                    />
                  ),
                  onClick: (id) => {
                    if (
                      window.confirm(
                        'Sigur mutati aceasta comanda in livrare ?'
                      )
                    )
                      switchToDelivery.mutate(
                        {
                          id,
                          order: { status: 'livrare' },
                        },
                        { onSucces: ordersRefetch }
                      );
                  },
                },
              ]}
            />
          )}
          <Pagination
            backwardText="Previous page"
            forwardText="Next page"
            itemsPerPageText="Items per page:"
            pageNumberText="Page Number"
            pageSize={5}
            pageSizes={[5]}
          />
        </div>
        <div className="bx--col"></div>
      </div>
      {open && (
        <Modal
          open={open}
          id={id}
          onRequestClose={() => setOpen(false)}
          onSecondarySubmit={() => setOpen(false)}
          title="Produse"
          cancelText="Inchide"
          type="edit"
        />
      )}
    </div>
  );
};

export default OrderList;
