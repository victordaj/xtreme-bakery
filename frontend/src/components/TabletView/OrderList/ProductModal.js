import React, { useEffect, useState } from 'react';
import {
  StructuredListBody,
  StructuredListCell,
  StructuredListHead,
  StructuredListWrapper,
  StructuredListRow,
} from 'carbon-components-react';
import Modal from '../../common/Modal/Modal';
import { useOrder } from '../../../hooks/index';

const headers = ['Nume', 'Cantitate'];

const ProductModal = (props) => {
  const [products, setProducts] = useState([]);
  const order = useOrder(props.id);

  if (order.isSuccess) {
    order.data.products.forEach((product) => {
      if (!products.filter((p) => p.name === product.product.name).length)
        setProducts([
          ...products,
          { quanitty: product.quantity, name: product.product.name },
        ]);
    });
  }

  return (
    <Modal
      open={props.open}
      title={props.title}
      cancelText={props.cancelText}
      onRequestClose={props.onRequestClose}
      onSecondarySubmit={props.onSecondarySubmit}
    >
      {order.isSuccess && (
        <StructuredListWrapper ariaLabel="Structured list">
          <StructuredListHead>
            <StructuredListRow head tabIndex={0}>
              {headers.map((header) => (
                <StructuredListCell head>{header}</StructuredListCell>
              ))}
            </StructuredListRow>
          </StructuredListHead>
          <StructuredListBody>
            {products &&
              products.map((product) => (
                <StructuredListRow tabIndex={0}>
                  <StructuredListCell>{product.name}</StructuredListCell>
                  <StructuredListCell>{product.quanitty}</StructuredListCell>
                </StructuredListRow>
              ))}
          </StructuredListBody>
        </StructuredListWrapper>
      )}
    </Modal>
  );
};

export default ProductModal;
