import React from 'react';

import PageHeader from '../common/PageHeader/PageHader';
import TabsMenu from '../common/TabsMenu/TabsMenu';
import { Wheat32 } from '@carbon/icons-react';

import List from './OrderList/OrderList';

const TabletView = () => {
  return (
    <div>
      <PageHeader title={'Comenzii in procesare'} icon={<Wheat32 />} />
      <TabsMenu
        tabs={[
          {
            label: 'Comenzii',
            component: <List />,
          },
        ]}
      />
    </div>
  );
};

export default TabletView;
