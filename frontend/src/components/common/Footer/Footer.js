import React from 'react';

import styles from './footer.module.scss';

const Footer = ({ children }) => (
  <div className={styles.footer}>{children}</div>
);

export default Footer;
