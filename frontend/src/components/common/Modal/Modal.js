import React from 'react';

import { Modal } from 'carbon-components-react';

import styles from './modal.module.scss';

const ModalWrapper = (props) => {
  return (
    <Modal
      open={props.open}
      modalHeading={props.title}
      primaryButtonText={props.acceptText || 'Add'}
      secondaryButtonText={props.cancelText || 'Cancel'}
      onRequestSubmit={props.onSubmit}
      onRequestClose={props.onRequestClose}
      onSecondarySubmit={props.onSecondarySubmit}
    >
      <div>{props.children}</div>
    </Modal>
  );
};

export default ModalWrapper;
