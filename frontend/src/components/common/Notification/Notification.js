import React from 'react';
import { InlineNotification, ToastNotification } from 'carbon-components-react';

const Notifications = (props) => {
  switch (props.type) {
    case 'inline':
      return (
        <InlineNotification
          kind={props.kind}
          subtitle={<span>{props.subtitle}</span>}
          title={props.title}
        />
      );
    case 'toast':
      return (
        <ToastNotification
          kind={props.kind}
          subtitle={<span>{props.subtitle}</span>}
          title={props.title}
          timeout={props.timeout}
        />
      );
    default:
      return (
        <InlineNotification
          kind={props.kind}
          subtitle={<span>{props.subtitle}</span>}
          title={props.title}
        />
      );
  }
};

export default Notifications;
