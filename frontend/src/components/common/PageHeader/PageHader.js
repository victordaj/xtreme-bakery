import React from 'react';


import styles from './page-header.module.scss';

const PageHeader = (props) => {
  return (
    <div>
      <div className={`${styles['pages-title']}`}>
        <h1>{props.title}</h1>
        <span>
          {props.icon}
        </span>
        {props.subtitle}
      </div>
    </div>
  );
};

export default PageHeader;
