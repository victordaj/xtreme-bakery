import React from 'react';

import {
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  TableHeader,
  DataTable,
  TooltipIcon,
  TableContainer,
  Table,
} from 'carbon-components-react';

const TableWrapper = (props) => {
  return (
    <>
      <DataTable
        rows={props.rows}
        headers={props.headers}
        className={props.className}
      >
        {({ rows, headers, getHeaderProps, getTableProps, getRowProps }) => (
          (
            <TableContainer title={props.title}>
              <Table {...getTableProps()}>
                <TableHead>
                  <TableRow>
                    {headers.map((header) => (
                      <TableHeader
                        {...getHeaderProps({ header })}
                        className={header.className}
                        key={`header-${header.content}`}
                      >
                        {header.content}
                      </TableHeader>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {props.rows.map((row) => (
                    <TableRow
                      {...getRowProps({ row })}
                      key={row.id}
                      className={row.className}
                    >
                      {row.cells.map((cell) => (
                        <TableCell
                          key={`cell-${row.id}-${cell.id}`}
                          className={cell.className}
                        >
                          {cell.value ? cell.value : ''}
                        </TableCell>
                      ))}
                      {props.actions && props.actions.length ? (
                        <TableCell className={props.actionCellClassName}>
                          {props.actions
                            .filter((action) =>
                              row.hideIf ? row.hideIf(action) : true
                            )
                            .map((action) => (
                              <TooltipIcon
                                key={`tooltip-${action.tooltip.text}`}
                                direction="top"
                                tooltipText={action.tooltip.text}
                                onClick={() => action.onClick(row.id)}
                              >
                                {action.icon}
                              </TooltipIcon>
                            ))}
                        </TableCell>
                      ) : null}
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          )
        )}
      </DataTable>
    </>
  );
};

export default TableWrapper;
