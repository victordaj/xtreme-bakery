import React from 'react';

import { Tabs, Tab } from 'carbon-components-react';
import styles from './tabs-menu.module.scss';

const TabsMenu = (props) => {
  return (
    <>
      <Tabs type="container" onSelectionChange={props.onChange}>  
        {props.tabs.map((node) => (
          <Tab
            id={`tab-${node.label}`}
            className={`${styles['tab-menu']}`}
            key={`tab-${node.label}`}
            label={node.label}
          >
            {node.component}
          </Tab>
        ))}
      </Tabs>
    </>
  );
};

export default TabsMenu;
