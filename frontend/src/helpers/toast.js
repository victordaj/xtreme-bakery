import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
toast.configure();

const configSuccess = {
  position: 'bottom-right',
};

const configError = {
  position: 'bottom-right',
};
const notification = {
  success: (message) => toast.success(message, configSuccess),
  error: (message) => toast.dark(message, configError),
};

export default notification;
