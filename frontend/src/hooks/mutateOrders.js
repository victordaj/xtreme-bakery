import { API } from '../helpers/index';
import { useMutation } from 'react-query';

const useCreateOrder = () =>
  useMutation((order) => API.post('/orders', { order: order }));

const useEditOrder = () =>
  useMutation((data) => API.put(`/orders/${data.id}`, { order: data.order }));

const useDeleteOrder = () =>
  useMutation((orderId) => API.del(`/orders/${orderId}`));

export { useCreateOrder, useEditOrder, useDeleteOrder };
