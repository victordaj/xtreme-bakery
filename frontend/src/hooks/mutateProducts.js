import { API } from '../helpers/index';
import { useMutation } from 'react-query';

const useCreateProduct = () =>
  useMutation((product) => API.post('/products', { product: product }));

const useEditProduct = () =>
  useMutation((product, productId) =>
    API.put(`/products/${productId}`, { product: product })
  );

const useDeleteProduct = () =>
  useMutation((productId) => API.del(`/products/${productId}`));

export { useCreateProduct, useEditProduct, useDeleteProduct };
