import { API } from '../helpers/index';
import { useMutation } from 'react-query';

const useCreateUser = () =>
  useMutation((user) => API.post('/users', { user: user }));

const useEditUser = () =>
  useMutation((user) => {
    return API.put(`/users/${user.id}`, { user: user.body });
  });

const useDeleteUser = () =>
  useMutation((userId) => API.del(`/users/${userId}`));

export { useCreateUser, useEditUser, useDeleteUser };
