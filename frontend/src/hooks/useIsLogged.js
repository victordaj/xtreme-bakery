import { API } from '../helpers/index';
import { useInfiniteQuery } from 'react-query';
import * as CONSTANTS from '../utils/constants';

const useIsLogged = () =>
  useInfiniteQuery(CONSTANTS.QUERY_TAGS.IS_LOGGED, () => API.get('/isLogged'));

export default useIsLogged;
