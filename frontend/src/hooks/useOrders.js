import { API } from '../helpers/index';
import { useQuery } from 'react-query';
import * as CONSTANTS from '../utils/constants';

const useOrder = (orderId, options) =>
  useQuery(CONSTANTS.QUERY_TAGS.ORDER_ONE, () =>
    API.get(`/orders/${orderId}`, { ...options })
  );

const useOrders = (query) =>
  useQuery(CONSTANTS.QUERY_TAGS.ORDER, () =>
    API.get(
      `/orders?from=${query.from || ''}&limit=${query.limit || ''}&filter=${
        query.filter || ''
      }`
    )
  );

export { useOrder, useOrders };
