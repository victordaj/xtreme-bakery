import { API } from '../helpers/index';
import { useQuery } from 'react-query';
import * as CONSTANTS from '../utils/constants';

const useProduct = (productId, options) =>
  useQuery(CONSTANTS.QUERY_TAGS.PRODUCT, () =>
    API.get(`/products/${productId}`, { ...options })
  );

const useProducts = (query) =>
  useQuery(CONSTANTS.QUERY_TAGS.PRODUCT, () =>
    API.get(`/products?from=${query.from || ''}&limit=${query.limit || ''}`)
  );

export { useProduct, useProducts };
