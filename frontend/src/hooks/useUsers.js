import { API } from '../helpers/index';
import { useQuery } from 'react-query';
import * as CONSTANTS from '../utils/constants';

const useUser = (userId, options) =>
  useQuery(CONSTANTS.QUERY_TAGS.USER, () =>
    API.get(`/users/${userId}`, { ...options })
  );

const useUsers = (query) =>
  useQuery(CONSTANTS.QUERY_TAGS.USER, () => API.get(`/users?from=${query.from || ''}&limit=${query.limit || ''}`));

export { useUser, useUsers };
