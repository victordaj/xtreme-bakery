import React from 'react';
import ReactDOM from 'react-dom';
import { QueryClientProvider, QueryClient } from 'react-query';
import '../src/styles/app.scss';
import '../src/styles/index.scss';
import Root from './components/Root';

const query = new QueryClient();

ReactDOM.render(
  <React.StrictMode>
    <QueryClientProvider client={query}>
      <Root />
    </QueryClientProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
