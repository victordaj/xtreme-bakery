import { errorHandler } from '../../utils/errorHandler'

import * as TYPES from '../actionTypes'

export const login = (email, password) => dispatch => axios.post('/auth', {
    email,
    password
}).then(response => {
    localStorage.setItem("tokenHelpdeskSolvvo", response.data.token)
    dispatch({
        type: TYPES.IS_LOGGED,
        payload: {
            username: `${response.data.user.name} ${response.data.user.surname}`,
            email: response.data.user.email,
            _id: response.data.user._id,
            role: response.data.user.role,
            phone: response.data.user.phone,
            licenseAccepted: response.data.user.licenseAccepted
        }
    })
    return Promise.resolve(response);
})

export const isLogged = () => dispatch => axios.get('/isLogged').then(decodedToken => {
    var loggedUser = decodedToken.data

    if (loggedUser) {
        dispatch({
            type: TYPES.IS_LOGGED,
            payload: {
                username: `${loggedUser.name} ${loggedUser.surname}`,
                email: loggedUser.email,
                _id: loggedUser._id,
                role: loggedUser.role,
                phone: loggedUser.phone,
                licenseAccepted: loggedUser.licenseAccepted
            }
        })
    }
}).catch(err => {
    localStorage.removeItem("tokenHelpdeskSolvvo")
    dispatch({
        type: TYPES.IS_NOT_LOGGED
    })
    return Promise.reject(err)
})