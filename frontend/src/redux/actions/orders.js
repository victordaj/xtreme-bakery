import axios from '../../utils/axios'
import { errorHandler } from '../../utils/errorHandler'

const ORDER_ROUTE = '/orders'

export const get = query => dispatch => axios.get(`${ORDER_ROUTE}?from=${query.from || ''}&limit=${query.limit || ''}&search=${query.search ? query.search : ''}&filter=${query.filter ? query.filter : ''}`)
    .then(response => ({ ...response.data }))
    .catch(errorHandler)

export const create = data => dispatch => axios.post(ORDER_ROUTE, { ...data })
    .then(response => ({ ...response.data }))
    .catch(errorHandler)

export const remove = id => dispatch => axios.delete(`${ORDER_ROUTE}/${id}`)
    .then(response => ({ ...response.data }))
    .catch(errorHandler)

export const edit = (id, data) => dispatch => axios.put(`${ORDER_ROUTE}/${id}`, { order: {...data}} )
    .then(response => ({ ...response.data }))
    .catch(errorHandler)