import axios from '../../utils/axios'
import { errorHandler } from '../../utils/errorHandler'

const PRODUCT_ROUTE = '/products'

export const get = query => dispatch => axios.get(`${PRODUCT_ROUTE}?from=${query.from || ''}&limit=${query.limit || ''}&search=${query.search ? query.search : ''}`)
    .then(response => ({ ...response.data }))
    .catch(errorHandler)

export const create = data => dispatch => axios.post(PRODUCT_ROUTE, { ...data })
    .then(response => ({ ...response.data }))
    .catch(errorHandler)

export const remove = id => dispatch => axios.delete(`${PRODUCT_ROUTE}/${id}`)
    .then(response => ({ ...response.data }))
    .catch(errorHandler)

export const edit = (id, data) => dispatch => axios.put(`${PRODUCT_ROUTE}/${id}`, { product: {...data}} )
    .then(response => ({ ...response.data }))
    .catch(errorHandler)