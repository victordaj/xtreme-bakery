import axios from '../../utils/axios'
import { errorHandler } from '../../utils/errorHandler'

const USER_ROUTE = '/users'

export const get = query => dispatch => axios.get(`${USER_ROUTE}?from=${query.from || ''}&limit=${query.limit || ''}&search=${query.search ? query.search : ''}`)
    .then(response => ({ ...response.data }))
    .catch(errorHandler)

export const create = data => dispatch => axios.post(USER_ROUTE, { ...data })
    .then(response => ({ ...response.data }))
    .catch(errorHandler)

export const remove = id => dispatch => axios.delete(`${USER_ROUTE}/${id}`)
    .then(response => ({ ...response.data }))
    .catch(errorHandler)

export const edit = (id, data) => dispatch => axios.put(`${USER_ROUTE}/${id}`, { user: {...data}} )
    .then(response => ({ ...response.data }))
    .catch(errorHandler)

