import { combineReducers } from 'redux'
import languageReducer from './language'
import loginReducer from './login'

export default combineReducers({
    languageReducer,
    loginReducer
})