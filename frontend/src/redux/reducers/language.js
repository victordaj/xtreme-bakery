import * as TYPES from '../actionTypes'

const initialState = {
   language: 'RO',
   i18n: require('../../utils/language/RO.json')
}

function changeLanguage(state = initialState, action) {
  switch (action.type) {
    case TYPES.LANGUAGE:
      return {...state, language: action.payload.lang, i18n: action.payload.i18n}
    default:
      return state
  }
}

export default changeLanguage