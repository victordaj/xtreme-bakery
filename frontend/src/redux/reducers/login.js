import * as TYPES from '../actionTypes'

const initialState = {
    isLogged: false,
    username: '',
    email: '',
    _id: '',
    role: '',
    phone: '',
    licenseAccepted: false
}

function loginReducer(state = initialState, action) {
    switch (action.type) {
        case TYPES.IS_LOGGED:
            return {
                ...state,
                isLogged: true,
                username: action.payload.name,
                email: action.payload.email,
                _id: action.payload._id,
                role: action.payload.role,
                phone: action.payload.phone,
                licenseAccepted : action.payload.licenseAccepted
            }
            case TYPES.IS_NOT_LOGGED:
                 return { ...state, isLogged: false }
            default:
                return state
        }
    }
    
    export default loginReducer