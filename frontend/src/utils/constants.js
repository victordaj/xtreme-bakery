export const BACKEND_URL = 'http://localhost:9000';

export const QUERY_TAGS = {
  USER: 'users',
  PRODUCT: 'products',
  ORDER: 'orders',
  ORDER_ONE: 'order-one',
  IS_LOGGED: 'is-logged',
};

export const ROLES = {
  WORKER: 'angajat',
  DELIVERY: 'livrator',
  ADMIN: 'admin',
};
